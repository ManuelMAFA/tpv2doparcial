﻿
namespace TPV2doParcial
{
    partial class Texto
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Esc = new System.Windows.Forms.Button();
            this.btn_Leer = new System.Windows.Forms.Button();
            this.rtxt_Contenido = new System.Windows.Forms.RichTextBox();
            this.txt_Direccion = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btn_Abrir = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_Esc
            // 
            this.btn_Esc.Location = new System.Drawing.Point(368, 306);
            this.btn_Esc.Name = "btn_Esc";
            this.btn_Esc.Size = new System.Drawing.Size(127, 29);
            this.btn_Esc.TabIndex = 1;
            this.btn_Esc.Text = "Escribir";
            this.btn_Esc.UseVisualStyleBackColor = true;
            this.btn_Esc.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn_Leer
            // 
            this.btn_Leer.Location = new System.Drawing.Point(368, 360);
            this.btn_Leer.Name = "btn_Leer";
            this.btn_Leer.Size = new System.Drawing.Size(127, 28);
            this.btn_Leer.TabIndex = 3;
            this.btn_Leer.Text = "Leer Archivo\r\n";
            this.btn_Leer.UseVisualStyleBackColor = true;
            this.btn_Leer.Click += new System.EventHandler(this.button2_Click);
            // 
            // rtxt_Contenido
            // 
            this.rtxt_Contenido.Location = new System.Drawing.Point(15, 59);
            this.rtxt_Contenido.Name = "rtxt_Contenido";
            this.rtxt_Contenido.Size = new System.Drawing.Size(325, 329);
            this.rtxt_Contenido.TabIndex = 4;
            this.rtxt_Contenido.Text = "";
            // 
            // txt_Direccion
            // 
            this.txt_Direccion.Location = new System.Drawing.Point(131, 23);
            this.txt_Direccion.Name = "txt_Direccion";
            this.txt_Direccion.Size = new System.Drawing.Size(364, 20);
            this.txt_Direccion.TabIndex = 5;
            this.txt_Direccion.TextChanged += new System.EventHandler(this.txt_Direccion_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Direccion Del Archivo:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btn_Abrir
            // 
            this.btn_Abrir.Location = new System.Drawing.Point(368, 81);
            this.btn_Abrir.Name = "btn_Abrir";
            this.btn_Abrir.Size = new System.Drawing.Size(127, 29);
            this.btn_Abrir.TabIndex = 7;
            this.btn_Abrir.Text = "Abrir";
            this.btn_Abrir.UseVisualStyleBackColor = true;
            this.btn_Abrir.Click += new System.EventHandler(this.button3_Click);
            // 
            // Texto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Aqua;
            this.ClientSize = new System.Drawing.Size(517, 442);
            this.Controls.Add(this.btn_Abrir);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_Direccion);
            this.Controls.Add(this.rtxt_Contenido);
            this.Controls.Add(this.btn_Leer);
            this.Controls.Add(this.btn_Esc);
            this.Name = "Texto";
            this.Text = "Informacion";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btn_Esc;
        private System.Windows.Forms.Button btn_Leer;
        private System.Windows.Forms.RichTextBox rtxt_Contenido;
        private System.Windows.Forms.TextBox txt_Direccion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btn_Abrir;
    }
}

